from tkinter import *

window = Tk()
window.title("Welcome to my app")
window.geometry('480x200')
lbl = Label(window, text="Hi :)\n" \
                    " This script can calculate your age in years, months and days!\n" \
                    "Enter your birth date (year, month, day),  e.g. 1994-02-22:", font=("Arial Bold", 12))
lbl.grid(column=0, row=0)
txt = Entry(window, width=10)
txt.grid(column=0, row=4)
txt.place(relx=.49, rely=.35, anchor="c")



def validate(input):
    msg = ''
    if not input:
        msg = "the list is empty"
    elif not len(input) == 10:
        msg = "Please, enter correct numbers"
    else:
        res = input.split("-")

        #import ipdb
        #ipdb.set_trace()
        birthday_year = res[0]
        birthday_month = res[1]
        birthday_day = res[2]
        try:
            for item in res:
                int(item)
        except:
            msg = "Please, enter a number"
        return res
    return msg


def clicked():
    input = txt.get()
    msg = validate(input)
    if not len(msg) == 3:
        lbl.configure(text=msg)
    else:

        res = "Your age is:\n" + input
        lbl.configure(text=res)
btn = Button(window, text="Calculate!", bg="Green", fg="black", command=clicked, font=("Arial Bold", 10))
btn.place(relx=.49, rely=.5, anchor="c")



window.mainloop()

