
from datetime import date
import calendar

import click


def bd_happened(dateNow, year, month, day):
    """Has birth day happened"""


    day_age = dateNow[2] - day
    month_age = dateNow[1] - month
    year_age = dateNow[0] - year


    if month < dateNow[1]:
        return True
    if month == dateNow[1]:
        if day - dateNow[2] <= 0:
            return True

    return False

def calc_time(year_age, month_age, day_age):

    date_now =[date.today().year, date.today().month, date.today().day]


    # These are the results we are going to print
    # Lets set them to None
    result_years = None
    result_months = None
    result_days = None

    # Check if user birthday has happened!

    if bd_happened(date_now, year_age, month_age, day_age):
        # If bd has happened keep year the same

        # monthrange Returns weekday of first day of the month
        # and number of days in month, for the specified year and month.
        days_in_current_month = calendar.monthrange(date.today().year, date.today().month)[1]

        # If the we are in the same month
        if date_now[1] == month_age:
            result_days = date.today().day - day_age
            result_years = date_now[0] - year_age
            result_months = 0

        else:
            # TODO handle case where months are not the same
            pass

    else:
        # B.day hasn't happened yet

        days_in_current_month = calendar.monthrange(date.today().year, date.today().month)[1]

        # If the we are in the same month
        if date_now[1] == month_age:
            result_days =  day_age - date.today().day
            result_years = date_now[0] - year_age - 1
            result_months = date_now[1] - 1

        else:
            # TODO handle case where months are not the same
            pass


    print ("Your age is ", (result_years, result_months, result_days))
    return ("Your age is ", (result_years, result_months, result_days))


@click.command()
@click.option('--year', default=1994, help='Year of birth')
@click.option('--month', default=12, help='Month of birth')
@click.option('--day', default=4, help='Day of birth')
def run_calculation(year, month, day):
    """Simple program that calculates your age"""
    click.echo('Year is %s!' % year)
    click.echo('Month is %s!' % month)
    click.echo('Day is %s!' % day)
    calc_time(int(year), int(month), int(day))


if __name__ == '__main__':
    run_calculation()
