import unittest

from calcuate_library import bd_happened

class TestBirthDayHappened(unittest.TestCase):

    """We want to test that the bd_day happend function returns true
    or false correctly"""

    def test_has_passed(self):
        """Date of birth has already occured"""

        date_now = [2018, 12, 23]

        year = 1994

        month = 12

        day = 4

        self.assertTrue(bd_happened(date_now, year, month, day))

    def test_has_not_passed(self):
        """Date of birth has not already passed"""

        date_now = [2018, 12, 1]

        year = 1994

        month = 12

        day = 4

        self.assertFalse(bd_happened(date_now, year, month, day))


    def test_has_same_day(self):
        """Date of birth has not already passed"""

        date_now = [2018, 12, 4]

        year = 1994

        month = 12

        day = 4

        self.assertTrue(bd_happened(date_now, year, month, day))

    def test_empty(self):
        pass


if __name__ == '__main__':
    unittest.main()