import tkinter
from tkinter import Tk, Label, Button, Entry

class MyFirstGUI:
    def __init__(self, master):
        self.master = master
        self.master.minsize(width=300, height=300)
        master.title("Age Calculation")
        self.user_info = tuple()

        self.text = "Hi! This script can calculate your age in years, months and days!\n" \
                    "Enter your birth date (year, month, day),  e.g. 1994, 02, 22:"

        self.label = Label(master, text=self.text)
        self.label.pack()

        self.input = Entry(self.master)
        self.input.pack()
        self.info = self.input.get()

        self.greet_button = Button(master, text="Calculate!", command=self.sanatize_input())
        self.greet_button.pack()



    def greet(self):
        self.label["text"] = "-----"

    def sanatize_input(self):

        if len(self.info) != 12:
            raise ValueError("Error we expect 12 characters")
        try:
            year = int(self.info[0:4])
        except:
            raise ValueError("Error we expect numbers, you hav entered letters for the year")


        try:
            month = int(self.info[6:8])
        except:
            raise ValueError("Error we expect numbers, you have entered letters for the month")

        try:
            day = int(self.info[10:12])
        except:
            raise ValueError("Error we expect numbers, you have entered letters for the day")



        self.user_info = (year, month, day)


    def bd_happened(dateNow, birthDayDate):
        day_age = dateNow[2] - birthDayDate[2]
        month_age = dateNow[1] - birthDayDate[1]
        year_age = dateNow[0] - birthDayDate[0]

        if month_age < 0:
            return True
        if month_age == 0:
            if day_age < 0:
                return True

        return False

    def get_num_days_in_month(year, month):
        return calendar.monthrange(year, month)

    def calc_time(user_birthdate):

        date_now = [date.today().year, date.today().month, date.today().day]
        print(date_now[0] - user_birthdate[0], date_now[1] - user_birthdate[1], date_now[2] - user_birthdate[2])

        year_age = date_now[0] - user_birthdate[0]
        month_age = 12 - user_birthdate[1] + date_now[1]

        last_month = 12 if date_now[1] == 1 else date_now[1] - 1
        last_year = date_now[0] - 1 if date_now[1] == 1 else date_now[0]
        last_month_days = get_num_days_in_month(last_year, last_month)
        day_age = last_month_days[1] - user_birthdate[2] + date_now[2]
        if bd_happened(date_now, user_birthdate):
            year_age -= 1
        if bd_happened(date_now, user_birthdate):
            month_age -= 1
        print("Your age is ", (year_age, month_age, day_age))

root = Tk()
my_gui = MyFirstGUI(root)
root.mainloop()